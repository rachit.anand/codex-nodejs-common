import { LoggerService } from "@nestjs/common";
import { KafkaService } from "./kafka.service";
import { DefaultConfig } from "../../config";
export declare class ReplayKafkaMessages {
    private readonly config;
    private readonly logger;
    private readonly kafkaService;
    constructor(config: DefaultConfig, logger: LoggerService, kafkaService: KafkaService);
    process(groupId: string, inTopic: string, outTopic: string): Promise<void>;
}
