import { LoggerService } from "@nestjs/common";
import { ConsumerServiceDelegateInterface } from "./consumer-service-delegate.interface";
import { DefaultConfig } from "../../config";
import { EventHandlerFactoryInterface } from "../../event/event.handler.factory.interface";
import { HealthzService } from "../healthz";
export declare class WebsocketConsumerService {
    private readonly healthzService;
    private readonly consumerDelegateService;
    private readonly config;
    private readonly logger;
    private readonly internalFactory;
    private topics;
    private eventHandler;
    constructor(healthzService: HealthzService, consumerDelegateService: ConsumerServiceDelegateInterface, config: DefaultConfig, logger: LoggerService, internalFactory: EventHandlerFactoryInterface);
    init(): Promise<void>;
    restart(topics: string[]): Promise<void>;
}
