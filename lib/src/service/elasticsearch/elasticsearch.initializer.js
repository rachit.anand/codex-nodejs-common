"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElasticsearchInitializer = void 0;
class ElasticsearchInitializer {
    constructor(logger, index, elasticsearchService, indexProperties, type) {
        this.logger = logger;
        this.index = index;
        this.elasticsearchService = elasticsearchService;
        this.indexProperties = indexProperties;
        this.type = type;
    }
    async _createIndex() {
        await this.elasticsearchService.indices.create({
            index: this.index,
            body: {},
        });
    }
    async _getIndexExists() {
        return await this.elasticsearchService.indices.exists({
            index: this.index,
        });
    }
    async initializeIndex() {
        await this._waitForReady();
        const exists = await this._getIndexExists();
        if (!exists) {
            await this._createIndex();
        }
        let params = {
            index: this.index,
            body: {
                properties: this.indexProperties,
            },
        };
        if (this.type) {
            params = Object.assign(Object.assign({}, params), { type: this.type });
        }
        await this.elasticsearchService.indices.putMapping(params);
    }
    async _waitForReady() {
        while (!(await this._isReady())) {
            this.logger.info("Waiting for elasticsearch");
            await new Promise((resolve) => {
                setTimeout(resolve, 1000);
            });
        }
    }
    async _isReady() {
        return new Promise(async (resolve) => {
            try {
                const result = await this.elasticsearchService.ping();
                resolve(result);
            }
            catch (e) {
                resolve(false);
            }
        });
    }
}
exports.ElasticsearchInitializer = ElasticsearchInitializer;
//# sourceMappingURL=elasticsearch.initializer.js.map