export interface WebsocketBroadcasterInterface {
    broadcast(topic: any, message: any): any;
}
