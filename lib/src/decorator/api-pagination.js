"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiPagination = void 0;
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
function ApiPagination() {
    return common_1.applyDecorators(...[
        swagger_1.ApiQuery({
            name: "page",
            required: true,
            example: 1,
        }),
        swagger_1.ApiQuery({
            name: "pageSize",
            required: true,
            example: 10,
        }),
    ]);
}
exports.ApiPagination = ApiPagination;
//# sourceMappingURL=api-pagination.js.map