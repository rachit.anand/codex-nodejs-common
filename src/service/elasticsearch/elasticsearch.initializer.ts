import { ElasticsearchService } from "@nestjs/elasticsearch";
import { CustomLogger } from "../../logger";
import { HttpStatus } from "@nestjs/common";

export class ElasticsearchInitializer {
  constructor(
    private readonly logger: CustomLogger,
    private readonly index: string,
    private readonly elasticsearchService: ElasticsearchService,
    private readonly indexProperties: any,
    private readonly type?: string
  ) {}

  private async _createIndex() {
    await this.elasticsearchService.indices.create({
      index: this.index,
      body: {},
    });
  }

  private async _getIndexExists() {
    return await this.elasticsearchService.indices.exists({
      index: this.index,
    });
  }

  async initializeIndex() {
    await this._waitForReady();

    const exists = await this._getIndexExists();
    if (!exists) {
      await this._createIndex();
    }

    let params: any = {
      index: this.index,
      body: {
        properties: this.indexProperties,
      },
    };
    if (this.type) {
      params = {
        ...params,
        type: this.type,
      };
    }
    await this.elasticsearchService.indices.putMapping(params);
  }

  async _waitForReady() {
    while (!(await this._isReady())) {
      this.logger.info("Waiting for elasticsearch");
      await new Promise((resolve) => {
        setTimeout(resolve, 1000);
      });
    }
  }
  async _isReady(): Promise<boolean> {
    return new Promise(async (resolve) => {
      try {
        const result = await this.elasticsearchService.ping();
        resolve(result);
      } catch (e) {
        resolve(false);
      }
    });
  }
}
