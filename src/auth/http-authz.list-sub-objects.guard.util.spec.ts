import { ExecutionContext } from "@nestjs/common";
import * as jwt from "jsonwebtoken";
import { HttpAuthzListSubObjectsGuardUtil } from "./http-authz.list-sub-objects.guard.util";

describe(HttpAuthzListSubObjectsGuardUtil.name, () => {
  it("Should allow super admin to list a group for a user", () => {
    const token = jwt.sign(
      {
        scopes: [`cool-app:::any:any:any:any:any:any`],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(true);
  });

  it("Should allow someone with permission to list groups for a user to list a group for the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:4d2114ca-24e2-43e5-bddb-d9a6688b8340::group:any:list`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(true);
  });

  it("Should allow someone with permission to do anything to any group on a user to list a group for the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:4d2114ca-24e2-43e5-bddb-d9a6688b8340::group:any:any`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(true);
  });

  it("Should allow someone with permission to do anything to any sub object for a user to list a group for the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(true);
  });

  it("Should allow someone with permission to list a specific group for a user to list the groups to the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:4d2114ca-24e2-43e5-bddb-d9a6688b8340::group::list`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(true);
  });

  it("Should not allow someone with permission to list groups to a different user to list a group for the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:55854a66-5a73-4416-b03a-eba4417b691c::group:any:create`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "001d4f53-798b-4a0b-8ef7-330a7bf72147",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(false);
  });

  it("Should not allow someone with permission to do anything to a different user to list a group for the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:55854a66-5a73-4416-b03a-eba4417b691c::group:any:any`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "001d4f53-798b-4a0b-8ef7-330a7bf72147",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(false);
  });

  it("Should not allow someone with permission to do anything to any sub object for a different user to list a group for the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:55854a66-5a73-4416-b03a-eba4417b691c::any:any:any`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "001d4f53-798b-4a0b-8ef7-330a7bf72147",
          },
          body: ["680dddec-f0b9-4a01-b8b5-be725f946935"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(false);
  });

  it("Should not allow someone with permission to list a different specific permission for a user to list the groups to the user", () => {
    const token = jwt.sign(
      {
        scopes: [
          `cool-app:::user:4d2114ca-24e2-43e5-bddb-d9a6688b8340::group:any:create`,
        ],
      },
      "hello"
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            userId: "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
          },
          body: ["5be3176f-c066-4418-b682-18e16fd07b84"],
        }),
      }),
    } as ExecutionContext;

    const util = new HttpAuthzListSubObjectsGuardUtil(context);

    expect(
      util.isAuthorized(
        "user",
        "4d2114ca-24e2-43e5-bddb-d9a6688b8340",
        "group",
        "cool-app"
      )
    ).toBe(false);
  });
});
