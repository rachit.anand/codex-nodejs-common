export class AuthorizationRequestInterface {
  object: string;
  objectId: string | number;
  action: string;
}
