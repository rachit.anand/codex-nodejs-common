import { HttpStatus } from "@nestjs/common";
import {
  LocaleInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";

export class HttpResponseMeta
  extends MessageMeta
  implements MessageMetaInterface {
  constructor(
    public readonly status: HttpStatus,
    type: string,
    locale: LocaleInterface,
    config: DefaultConfig,
    correlationId: string,
    started: Date
  ) {
    super(type, locale, config, correlationId, started);
  }

  public toJSON(): any {
    return {
      status: this.status,
      type: this.type,
      schemaVersion: this.schemaVersion,
      correlationId: this.correlationId,
      time: this.time,
      context: this.context,
      locale: this.locale,
      client: this.client,
    };
  }
}
