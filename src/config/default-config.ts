import { CountryCode, LanguageCode } from "@cryptexlabs/codex-data-model";
import { MetricsHostConfig } from "./metrics-host-config";
import { HostConfigInterface } from "./host-config.interface";
import { SwitchConfigInterface } from "./switch-config.interface";
import * as fs from "fs";
import { config, parse } from "dotenv";
import { KafkaConfig } from "./kafka-config";
import { KafkaConfigInterface } from "./kafka-config.interface";
import { JwtConfig } from "./jwt-config";
import { JwtConfigInterface } from "./jwt-config.interface";
import { Injectable } from "@nestjs/common";
import { UrlInterface } from "./url.interface";
import { ElasticsearchConfig } from "./elasticsearch-config";
import { PingConfigInterface } from "./ping-config.interface";
import { ConsumerTypeEnum } from "./consumer-type.enum";
import { JsonSerializableInterface } from "../message";

@Injectable()
export class DefaultConfig implements JsonSerializableInterface<any> {
  private _appName: string;
  private _packageVersion: string;
  private _appPath: string;

  constructor(
    basePath: string,
    appPath: string,
    private readonly _clientId: string,
    fallbackEnvironment?: string
  ) {
    this.setup(
      basePath,
      appPath,
      process.env.ENV_NAME || fallbackEnvironment || ""
    );
    this._appPath = appPath;
  }

  public toJSON() {
    return {
      fallbackLanguage: this.fallbackLanguage,
      fallbackCountry: this.fallbackCountry,
      appVersion: this.appVersion,
      appName: this.appName,
      clientId: this.clientId,
      apiVersion: this.apiVersion,
      appPrefix: this.appPrefix,
      docsPrefix: this.docsPrefix,
      docsEnabled: this.docsEnabled,
      docsPath: this.docsPath,
      kafka: this.kafka,
      environmentName: this.environmentName,
      logLevels: this.logLevels,
      httpPort: this.httpPort,
      metrics: this.metrics,
      jwt: this.jwt,
      healthzFilePath: this.healthzFilePath,
      elasticsearch: this.elasticsearch,
      consumerType: this.consumerType,
      gpuEnabled: this.gpuEnabled,
      dataDirectory: this.dataDirectory,
    };
  }

  protected setup(basePath: string, appPath: string, environment: string) {
    const envBasePath = `${basePath}/env`;
    const configPath = `${envBasePath}/${environment}.env`;
    if (fs.existsSync(configPath)) {
      config({ path: configPath });
    }

    const secretsPath = `${envBasePath}/${environment}${
      environment ? "." : ""
    }secrets.env`;
    if (fs.existsSync(secretsPath)) {
      const envConfig = parse(fs.readFileSync(secretsPath));

      for (const k in envConfig) {
        process.env[k] = envConfig[k];
      }
    }

    const configOverridePath = `${envBasePath}/${environment}.override.env`;
    if (fs.existsSync(configOverridePath)) {
      const envConfig = parse(fs.readFileSync(configOverridePath));

      for (const k in envConfig) {
        process.env[k] = envConfig[k];
      }
    }

    const packageJson = require(`${appPath}/package.json`);
    this._appName = packageJson.name;
    this._packageVersion = packageJson.version;
  }

  public get fallbackLanguage(): LanguageCode {
    return process.env.LOCALE_LANGUAGE_FALLBACK as LanguageCode;
  }

  public get fallbackCountry(): CountryCode {
    return process.env.LOCALE_COUNTRY_FALLBACK as CountryCode;
  }

  public get appVersion(): string {
    return process.env.APP_VERSION || this._packageVersion;
  }

  public get appName(): string {
    return this._appName;
  }

  public get clientId(): string {
    return this._clientId;
  }

  public get apiVersion(): string {
    return process.env.API_VERSION || "v1";
  }

  public get appPrefix(): string {
    return process.env.APP_PREFIX || "api";
  }

  public get docsPrefix(): string {
    return process.env.DOCS_PREFIX || "docs";
  }

  public get docsEnabled(): boolean {
    return process.env.DOCS_ENABLED === "true";
  }

  public get docsPath(): string {
    return process.env.DOCS_PATH || "";
  }

  public get kafka(): KafkaConfigInterface {
    return new KafkaConfig(
      process.env.KAFKA_ENABLED,
      process.env.KAFKA_HOST,
      process.env.KAFKA_PORT,
      process.env.KAFKA_USERNAME,
      process.env.KAFKA_PASSWORD,
      process.env.SASL_MECHANISM,
      process.env.KAFKA_TOPICS_CONFIG_PATH ||
        `${this._appPath}/config/topics.yaml`,
      process.env.KAFKA_CLIENT_ID || process.env.POD_NAME || this.appName,
      process.env.KAFKA_DEFAULT_PARTITIONS,
      process.env.KAFKA_DEFAULT_ERROR_TOPIC
    );
  }

  public get environmentName(): string {
    return process.env.ENV_NAME;
  }

  public get logLevels(): string[] {
    return (process.env.LOG_LEVELS || "debug,info,error").trim().split(",");
  }

  public get httpPort(): number {
    return parseInt(process.env.HTTP_PORT || "3000", 10);
  }

  public get metrics(): SwitchConfigInterface & HostConfigInterface {
    return new MetricsHostConfig(
      process.env.GRAPHITE_HOST,
      process.env.GRAPHITE_PORT,
      process.env.METRICS_ENABLED
    );
  }

  public get jwt(): JwtConfigInterface {
    return new JwtConfig(process.env.JWT_VERIFY, process.env.JWT_AUD);
  }

  public get healthzFilePath(): string {
    return process.env.HEALTHZ_FILE_PATH || "/tmp/healthz";
  }

  public get elasticsearch(): UrlInterface & PingConfigInterface {
    return new ElasticsearchConfig(
      process.env.ELASTICSEARCH_URL || "http://elasticsearch:9200",
      process.env.ELASTICSEARCH_PING_INTERVAL_SECONDS || "10"
    );
  }

  public get consumerType(): ConsumerTypeEnum {
    return (
      (process.env.CONSUMER_TYPE as ConsumerTypeEnum) ||
      ConsumerTypeEnum.DEFAULT
    );
  }

  public get gpuEnabled(): boolean {
    return process.env.GPU_ENABLED === "true";
  }

  public get dataDirectory(): string {
    return process.env.DATA_DIRECTORY || "/data";
  }
}
