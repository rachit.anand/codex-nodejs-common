import { ConnectionConfigInterface } from "./connection-config.interface";
import { EndpointConfig } from "./endpoint-config";

export class ConnectionConfig
  extends EndpointConfig
  implements ConnectionConfigInterface {
  protocol: string;

  get connectionUrl() {
    return `${this.protocol}://${this.endpoint}`;
  }
}
