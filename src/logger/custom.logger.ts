import { Injectable, Logger, LoggerService } from "@nestjs/common";

@Injectable()
export class CustomLogger implements LoggerService {
  private readonly logger: LoggerService;
  private readonly filterContext: string[];

  constructor(
    private defaultContext: string = "info",
    filterContext: string[] = [],
    isTimestampEnabled?: boolean,
    logger?: LoggerService
  ) {
    this.filterContext = filterContext;

    if (logger) {
      this.logger = logger;
    } else {
      this.logger = new Logger(defaultContext, {
        timestamp: isTimestampEnabled,
      });
    }
  }

  public log(message: string, context: string = "") {
    if (context === "") {
      context = this.defaultContext;
    }
    if (this.filterContext.length > 0) {
      if (this.filterContext.includes(context)) {
        this.logger.log(message, context);
      }
    } else {
      this.logger.log(message, context);
    }
  }

  public error(
    error: string | Error,
    trace: string,
    context: string = "error"
  ) {
    let message;
    let useTrace;
    if (error instanceof Error) {
      message = error.message;
      useTrace = error.stack;
    } else {
      message = error;
      useTrace = trace;
    }

    if (this.filterContext.length > 0) {
      if (this.filterContext.includes(context)) {
        this.logger.error(message, useTrace, context);
      }
    } else {
      this.logger.error(message, useTrace, context);
    }
  }

  public debug(message: any, context: string = "debug"): any {
    this.log(message, context);
  }

  public verbose(message: any, context: string = "verbose"): any {
    this.log(message, context);
  }

  public warn(message: any, context: string = "warn"): any {
    this.log(message, context);
  }

  public info(message: any, context: string = "info"): any {
    this.log(message, context);
  }
}
